Blacknet.controller('index', ['$scope', '$http', function ($scope, $http) {
    
}]);

Blacknet.controller('recent_blocks', ['$scope', '$http', '$timeout', function ($scope, $http, $timeout) {

    $scope.txns = [];
    function update(){
        $http.get('/api/recent_blocks').then(function (res) {
            $scope.txns = res.data;
        });
        $timeout(update, 1000 * 30);
    }
    update();

}]);

Blacknet.controller('workers', ['$scope', '$http', '$timeout',function ($scope, $http, $timeout) {

    $scope.workers = [];
    function update(){
        $http.get('/api/workers').then(function (res) {
            $scope.workers = res.data;
        })
        $timeout(update, 1000 * 30);
    }
    update();
}]);

Blacknet.controller('top_accounts', ['$scope', '$http', function ($scope, $http) {
    $scope.top_accounts = [];
    $http.get('/api/top_accounts' + location.search).then(function (res) {
        $scope.top_accounts = res.data;
    });
    $http.get('/api/top_accounts_chardata' + location.search).then(function (res) {
        var params = {
            title: i18nData[res.data.params.title] || res.data.params.title,
            name: i18nData[res.data.params.name] || res.data.params.name
        }
        var chart = {
            backgroundColor: "#3B3B3B",
            plotBackgroundColor: "#3B3B3B",
            plotBorderWidth: null,
            plotShadow: false
        };
        var title = {
            text: params.title,
            style: { "color": "#e8ba3f", "fontSize": "18px" }
        };      
        var tooltip = {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        };
        var plotOptions = {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || '#adadad'
                    }
                }
            }
        };
        var series= [{
            type: 'pie',
            name: params.name,
            data: res.data.chart}]; 
        var json = {
            credits:{
                enabled: false
            }
        };   
        json.chart = chart; 
        json.title = title;     
        json.tooltip = tooltip;  
        json.series = series;
        json.plotOptions = plotOptions;
        $('.chart').highcharts(json);  
    });
}]);

Blacknet.controller('transaction', ['$scope', '$http', function ($scope, $http) {
    $scope.tx = {
        amount: 0,
        fee: 0
    };
    $http.get('/api' + location.pathname).then(function (res) {
        $scope.tx = res.data;
    });
}]);

Blacknet.controller('blockController', ['$scope', '$http', function ($scope, $http) {
    $scope.block = {};
    $http.get('/api' + location.pathname).then(function (res) {
        res.data.transactions = res.data.transactions.map(function(tx){
            if(typeof tx.data == 'string') tx.data = JSON.parse(tx.data);
            return tx;
        });
        $scope.block = res.data;
    });
}]);

Blacknet.controller('account', ['$scope', '$http', function ($scope, $http) {
    $scope.account = {
        balance: 0,
        confirmedBalance: 0,
        stakingBalance: 0
    };
    $scope.txns = [];

    $http.get('/api' + location.pathname + location.search).then(function (res) {
        $scope.account = res.data.account;
        $scope.txns = res.data.txns.map(function(tx){
            if(typeof tx.data == 'string') tx.data = JSON.parse(tx.data);
            return tx;
        });
        $scope.page = res.data.page;
        $scope.pager = res.data.pager;
        $scope.page_number = res.data.page_number;
        $scope.filter_pos_generated = res.data.filter_pos_generated == 1 ? true : false;
    });

    $scope.changeUrl = function () {

        location.href = location.pathname + '?page=1&filter_pos_generated=' + ($scope.filter_pos_generated ? 1 : 0);
    }
}]);
Blacknet.controller('bodyController', ['$scope', '$http', '$timeout', function ($scope, $http, $timeout) {

    let lang = navigator.language || navigator.userLanguage, locale = '';

    $scope.BlockTime = BlockTime;
    
    if (lang.indexOf('zh') !== -1) {
        locale = 'zh';
    } else if (lang.indexOf('ja') !== -1) {
        locale = 'ja';
    } else if (lang.indexOf('sk') !== -1) {
        locale = 'sk';
    }
    if (locale) {
        $http.get('/i18n/' + locale + '.json').then(function (res) {
            let data = res.data;
            for (let key in data) {
                data[key.toLowerCase()] = data[key];
            }
            window.i18nData = data
            $(document).find('[data-i18n]').each(function () {

                let el = $(this), key = el.data('i18n').toLowerCase();

                if (data[key]) {
                    el.text(data[key]);
                }
            });
            
            $timeout(function(){
                $('.dialog').hide();
            }, 200);

        });
    }else{
        $timeout(function(){
            $('.dialog').hide();
        }, 200);
    }
}]);

