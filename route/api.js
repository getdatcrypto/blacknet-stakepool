


const Router = require('koa-router');
const router = new Router({
    prefix: '/api'
});
const BigNumber = require('bignumber.js');

router.get('/workers', async (ctx, next) => {
    
    ctx.body = await Worker.find({}).sort({hashrate: 'desc'});;
});

router.get('/recent_blocks', async (ctx, next) => {
    let txns = await Transaction.find({type: 254}).limit(100)
    ctx.body = txns;
});

router.get('/recent_transactions', async (ctx, next) => {

    ctx.body = await Transaction.find({ type: {$not: { $gt: 253 }} }).limit(50).sort({ time: 'desc' });
});


router.get('/tx/:hash', async (ctx, next) => {
   
    if(!Utils.verifyHash(ctx.params.hash)){
        return next()
    }

    let hash = ctx.params.hash, query = { txid: hash.toUpperCase() };

    let tx = await Transaction.findOne(query);
    
    if(!tx){
        return next()
    }
    tx.amountStr = toAmountStr(tx.amount)
    tx.feeStr = toAmountStr(tx.fee)
    tx.typeStr = getTxType(tx.type);


    ctx.body = tx;
});

router.get('/top_accounts', async (ctx, next) => {

    let order = ctx.query.order_by || 'rich', accounts, chartData=[], supply= overview.supply, params={};

    if(order == 'rich'){
        query = { balance: 'desc' };
    }else if(order == 'pos'){
        query = { blocks: 'desc'};
    }else{
        query = { stakingBalance: 'desc'};
    }
   
    ctx.body = await Account.find({}).limit(100).sort(query);;
});

router.get('/top_accounts_chardata', async (ctx, next) => {

    let order = ctx.query.order_by || 'rich', accounts, chartData=[], supply= overview.supply, params={};

    if(order == 'rich'){
        query = { balance: 'desc' };
        params = {
            title: "",
            name: "Balance"
        }
    }else if(order == 'pos'){
        query = { blocks: 'desc'};
        params = {
            title: "",
            name: "Blocks"
        }
        // 计算总量
        let count = await Account.aggregate([{$match: {blocks: { $gt: 0 }}}, {$group : {_id : null, totalBlocks : {$sum : "$blocks"}}}])
        if(count[0] && count[0].totalBlocks) {
            supply = count[0].totalBlocks
        }
    }else{
        query = { stakingBalance: 'desc'};
        params = {
            title: "",
            name: "Balance"
        }
        // 计算总量
        let count = await Account.aggregate([{$match: {stakingBalance: { $gt: 0 }}}, {$group : {_id : null, stakingBalance : {$sum : "$stakingBalance"}}}])
        if(count[0] && count[0].stakingBalance) {
            supply = count[0].stakingBalance
        }
    }
   
    accounts = await Account.find({}).limit(100).sort(query);
    // accounts = accountser(accounts);

    chartData = formartAccountChartData(accounts,supply,order)

    ctx.body = {
        chart: chartData,
        params: params
    };
});


function formartAccountChartData(accounts, supply, type) {
    var chartdata = []
    supply = new BigNumber(supply)
    var total = new BigNumber(0)
    accounts.map(account => {
        switch (type) {
            case "rich":
                var balance = new BigNumber(account.balance).dividedBy(1e8)
                chartdata.push([account.address, balance.toNumber()])
                total = BigNumber.sum.apply(null, [total, balance])
                break;
            case "pos":
                var blocks = new BigNumber(account.blocks)
                chartdata.push([account.address, blocks.toNumber()])
                total = BigNumber.sum.apply(null, [total, blocks])
            default:
                var stakingBalance = new BigNumber(account.stakingBalance)
                chartdata.push([account.address, stakingBalance.toNumber()])
                total = BigNumber.sum.apply(null, [total, stakingBalance])
                break;
        }
    });
    chartdata.push(["Other", parseFloat(supply - total)])
    return chartdata
}



router.get('/block/:height', async (ctx, next) => {
    let block;
    if(Utils.verifyBlockNumber(ctx.params.height)){
        block = await Block.findOne({ height: ctx.params.height });
    }else if(Utils.verifyHash(ctx.params.height)){
        block = await Block.findOne({ blockHash: String(ctx.params.height).toUpperCase() });
    }else{
        return next()
    }

    if(!block){
        return await ctx.render('404', {msg: "Block not found"})
    }

    let tx = await Transaction.findOne({type: 254, blockHash: block.blockHash});
    let txns = [tx].concat(block.transactions);
    block.transactions = txns;
    ctx.body = block;
});


router.get('/block/lastest', async (ctx, next) => {

    let blocks = await Block.find({}).limit(50).sort({ time: 'desc' });

    ctx.body = blocks;
});

router.get('/account/:address', async (ctx, next) => {
    if(!Utils.verifyAccount(ctx.params.address)){
        return next()
    }
    let txns = [], count = 0, page = ctx.query.page || 1, page_number = 0, pager = [];
    let address = ctx.params.address.toLowerCase(), query = { $or: [{ from: address }, { to: address }] };
    let filter_pos_generated = ctx.query.filter_pos_generated || 0;
    let account = await Account.findOne({ address: address });

    if(filter_pos_generated == '1'){
        query.type = {$not: {$eq: 254}};
    }

    if(!account){
        account = {
            balance: 0,
            address,
            confirmedBalance: 0,
            stakingBalance: 0,
            txamount: 0
        }
    }else{
        txns = await Transaction.find(query).skip( (page-1)*100).limit(100).sort({ time: -1 }).lean();
        account.txamount = await Transaction.count(query);
        account.balancestr = new BigNumber(account.balance).dividedBy(1e8).toFixed(8);
    }

    if(txns.length == 100 || page > 1){
        count = await Transaction.count(query);
        page_number = Math.ceil(count / 100);
        pager = getPager(+page, page_number);
    }

    ctx.body = {
        txns,
        account,
        page_number,
        filter_pos_generated, pager, page
    }
});

router.get('/account/richlist', async (ctx, next) => {

    let blocks = await Account.find({}).limit(50).sort({ balance: 'desc' });

    ctx.body = blocks;
});

module.exports = router;

/**
 * format amount to str 
 * @method toAmountStr
 * @param {number} amount
 * @return {string} str
 */
function toAmountStr(amount) {
    return new BigNumber(amount).dividedBy(1e8).toFixed(8);
}


function accountser(accounts) {
    var supply = new BigNumber(overview.supply).times(1e8)
    return accounts.map(account => {
        account.balancestr = new BigNumber(account.balance).dividedBy(1e8).toFixed(8);
        account.percent = new BigNumber(account.balance).dividedBy(supply).times(1e2).toFixed(8);
        return account;
    });
}

var txType = ["Transfer", "Burn", "Lease", "CancelLease", "Bundle", "CreateHTLC",
    "UnlockHTLC", "RefundHTLC", "SpendHTLC", "CreateMultisig", "SpendMultisig"];

    txType[254] = "Pos Generated";

function getTxType(type){

    return txType[type] || 'genesis';
}

function getPager(now, max){
    
    let arr = [now -3, now -2, now -1, now, now + 1, now + 2, now + 3];

    arr = arr.filter((page)=>{

        if( page < 1 ) return false;

        if( page > max ) return false;

        return true;
    });

    return arr;
}